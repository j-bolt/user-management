var app = require('express')();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var dotenv = require('dotenv').config();
var db = mongoose.connect(process.env.MONGODB_URL);
var User = require('./models/userModel');
var port = process.env.PORT || 9000;
var userRouter = require('./routes/userRoutes')(User);

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api/v1/users', userRouter);

app.get('/', function (req, res) {
    res.send('User Management REST Services 0.0.1');
});

app.listen(port, function () {
    console.log('Server Started on Port: ' + port);
});
